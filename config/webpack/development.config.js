const webpack = require('webpack');
const webpackMerge = require('webpack-merge');
import { commonConfig } from './common.config';

export let developmentConfig;

developmentConfig = {
	devServer: {
		historyApiFallback: true,
		hot: true,
		open: true,
		stats: {
			colors: true
		},
		progress: true
	},
	plugins: [
		new webpack.NamedModulesPlugin(),
		new webpack.HotModuleReplacementPlugin()
	]
};
developmentConfig = webpackMerge( commonConfig, developmentConfig );