import {CUSTOM_ELEMENTS_SCHEMA, ErrorHandler, NgModule} from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';

import { RollbarModule, RollbarService } from 'angular-rollbar'

import { AppComponent } from './app.component';

import { PagesModule } from "./pages/pages.module";
import { FooterModule } from "./layout/footer/footer.module";
import { HeaderModule } from "./layout/header/header.module";

@NgModule({
	imports: [
		BrowserModule,
		FormsModule,

		RollbarModule.forRoot({
			accessToken: '168ca3aa207f4013840b1a3838d0e2e1'
		}),

		FooterModule,
		HeaderModule,
		PagesModule,
		RouterModule
	],
	declarations: [
		AppComponent
	],
	providers: [
		{ provide: ErrorHandler, useClass: RollbarService }
	],
	bootstrap: [
		AppComponent
	],
	schemas: [ CUSTOM_ELEMENTS_SCHEMA ]
})
export class AppModule { }