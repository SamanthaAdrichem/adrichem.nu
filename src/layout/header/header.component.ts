import {Component, ElementRef, OnInit} from '@angular/core';

@Component({
	selector: 'app-header',
	templateUrl: 'templates/header.html',
	styleUrls: ['styles/header.scss']
})
export class HeaderComponent implements OnInit {

	private elementReference: ElementRef = null;
	private flipTimeout: number = 5000;
	private flipClass: string = 'header-container__logo-container--flipped';
	private flipContainerSelector: string = '.header-container__logo-container';

	constructor(elementReference: ElementRef) {
		this.elementReference = elementReference;
	}

	ngOnInit() {
		// setInterval(this.flipLogo.bind(this), this.flipTimeout);
	}

	flipLogo() {
		let containerClasses = this.elementReference.nativeElement.querySelector(this.flipContainerSelector).classList;
		if (containerClasses.contains(this.flipClass)) {
			containerClasses.remove(this.flipClass);
		} else {
			containerClasses.add(this.flipClass);
		}
	}

}