import {Component} from '@angular/core';

@Component({
	selector: 'app-footer',
	templateUrl: 'templates/footer.html',
	styleUrls: ['styles/footer.scss']
})
export class FooterComponent {}