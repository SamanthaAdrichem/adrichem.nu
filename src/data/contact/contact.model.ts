export class Contact {
	name: string;
	email: string;
	message: string;
	'g-recaptcha-response': string;
}