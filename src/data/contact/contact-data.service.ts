import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { Contact } from "./contact.model";
import { HttpClient } from "@angular/common/http";

@Injectable()
export class DataContactDataService {

	private httpClient: HttpClient = null;
	private serviceUrl: string = "https://formcarry.com/s/B1CQHo5WM";

	constructor(httpClient: HttpClient) {
		this.httpClient = httpClient;
	}

	saveContact( contact: Contact): Observable<Object> {
		let body = new FormData();
		for ( var key in contact ) {
			if (contact.hasOwnProperty( key )) {
				body.append(key, contact[key]);
			}
		}
		return this.httpClient.post(
			this.serviceUrl,
			body,
			{
				headers: {
					'Accept': 'application/json'
				}
			}
		);
	}
}