import { NgModule } from '@angular/core';
import { DataContactDataService } from "./contact-data.service";
import { HttpClientModule } from "@angular/common/http";

@NgModule({
	imports: [
		HttpClientModule
	],
	providers: [
		DataContactDataService
	]
})
export class DataContactDataModule {}