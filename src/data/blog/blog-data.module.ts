import { NgModule } from '@angular/core';
import { DataBlogDataService } from "./blog-data.service";

@NgModule({
	providers: [
		DataBlogDataService
	]
})
export class DataBlogDataModule {}