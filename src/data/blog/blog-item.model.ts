export class BlogItem {
	id: number;
	title: string;
	date: Date;
	description: string;
}