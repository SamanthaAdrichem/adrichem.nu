import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { of } from 'rxjs/observable/of';
import { BlogItem } from "./blog-item.model";

const BLOG_ITEMS: BlogItem[] = [
	{
		id: 1,
		title: 'Webkit hell',
		date: new Date(2017, (12 - 1), 11, 17, 41, 0, 0),
		description: '<p>Just like Gulp and Grunt, Webkit is a powerful package to build your frontend project with modular components. Unlike Gulp and Grunt however, webkit starts with a single entry point and works out of the box if you only use javascript.</p>'
		 + '<p>Recently I\'ve started using webkit for an office project to replace Gulp. And that\'s where it all started. Even though webkit is an awesome product, it has quite a learning curve. Most solutions on the internet still refer to webkit 2 or even webkit 1 and the same goes for Angular 5 vs AngularJs</p>'
		 + '<p>Adding the pressure of having to modify almost 7.000 files (6.941 to be precise) to add import and require statements to all our AngularJs files and finally fix the module dependency tree I came to the point of not wanting to learn anything new or change a single bit.</p>'
		 + '<p>It all started with a nice view on creating a small configuration setup split into pieces, since the gulpfile was huge as well, i started searching on google for finding a way to split webpack into parts. There I learned of <a href="https://github.com/andywer/webpack-blocks" rel="noreferer noopener" target="_blank">webpack-blocks</a> a package that allows you to build webpack using blocks.</p>'
		 + '<p>Webpack blocks surely makes your config neat and tidy and it allows for easier file pattern matching using the Gulp like match syntax. However in the project at work it still resulted in a huge file of a whopping 338 lines. This partly due to having to copy lazy-load files that arn\'t require compatible yet and needing to run an AngularJs and Angular hybrid solution.</p>'
		 + '<p>After all this was finished and after being grumpy and making others grumpy, I had to start a project at home for my studies. I decided to build this website in Angular and use webpack, even though I didn\'t like configuring it, I did like the result.</p>'
		 + '<p>This time however I had all the time in the world and I decided to not use webpack blocks but learn native webpack and split my config in multiple files. The Angular startup repository which was half broken showed me it should be possible using webpack merge.</p>'
		 + '<p>After lots of research I managed to put every single config file, outside of package.json, in the config folder, away from the ugly root of the project. I managed to split every single loader into separate files and I managed to setup a basic modular Angular project</p>'
		 + '<p>The new structure made me kinda proud and showing it at the office resulted in implementing it and even improving on it. In the office project, the copying of the lazy load translation files is now included in the translation loader, and the copying of the static files is now in the file loader.</p>'
		 + '<p>Since I\'ve been struggling so much I\'ve decided to put a small <a href="https://github.com/SamanthaAdrichem/webpack-3.9.1-splitted-config-angular" rel="noreferer noopener" target="_blank">demo repository</a> online and share it with the world. Check it out <a href="https://github.com/SamanthaAdrichem/webpack-3.9.1-splitted-config-angular" rel="noreferer noopener" target="_blank">here</a> and be sure to let me know about improvements.</p>'
	}
];

@Injectable()
export class DataBlogDataService {

	constructor() { }

	get(): Observable<BlogItem[]> {
		return of(BLOG_ITEMS)
	}

	getById( itemId: number ): Observable<BlogItem[]> {
		return of(BLOG_ITEMS.filter(blogItem => blogItem.id === itemId));
	}


}