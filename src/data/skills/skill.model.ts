export class Skill {
	name: string;
	image: string;
	description: string;
	stars: number;
}