import { NgModule } from '@angular/core';
import { DataSkillsDataService } from "./skills-data.service";

@NgModule({
	providers: [
		DataSkillsDataService
	]
})
export class DataSkillsDataModule {}