import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { of } from 'rxjs/observable/of';
import { Skill } from "./skill.model";

const SKILLS: Skill[] = [
	{
		name: 'Angular',
		description: 'Angular is a platform that makes it easy to build applications with the web. Angular combines declarative templates, dependency injection, end to end tooling, and integrated best practices to solve development challenges. Angular empowers developers to build applications that live on the web, mobile, or the desktop',
		image: require('images/skills/angular.png'),
		stars: 1
	},
	{
		name: 'AngularJs',
		description: 'AngularJS is a structural framework for dynamic web apps. It lets you use HTML as your template language and lets you extend HTML\'s syntax to express your application\'s components clearly and succinctly. AngularJS\'s data binding and dependency injection eliminate much of the code you would otherwise have to write. And it all happens within the browser, making it an ideal partner with any server technology.',
		image: require('images/skills/angular-js.png'),
		stars: 5
	},
	{
		name: 'Bem',
		description: ' Block Element Modifier is a CSS class naming methodology that helps you to create reusable components and code sharing in front-end development.',
		image: require('images/skills/bem.jpg'),
		stars: 4
	},
	{
		name: 'CSS3',
		description: 'CSS3 is the latest evolution of the Cascading Style Sheets language and aims at extending CSS2.1. It brings a lot of long-awaited novelties, like rounded corners, shadows, gradients, transitions or animations, as well as new layouts like multi-columns, flexible box or grid layouts. Experimental parts are vendor-prefixed and should either be avoided in production environments, or used with extreme caution as both their syntax and semantics can change in the future.',
		image: require('images/skills/css.png'),
		stars: 3
	},
	{
		name: 'HTML5',
		description: 'HTML5 is a markup language used for structuring and presenting content on the World Wide Web. It is the fifth and current major version of the HTML standard.',
		image: require('images/skills/html.png'),
		stars: 4.5
	},
	{
		name: 'Java',
		description: 'Java is a general-purpose computer programming language that is concurrent, class-based, object-oriented, and specifically designed to have as few implementation dependencies as possible.',
		image: require('images/skills/java.png'),
		stars: 2
	},
	{
		name: 'Javascript',
		description: 'JavaScript® (often shortened to JS) is a lightweight, interpreted, object-oriented language with first-class functions, and is best known as the scripting language for Web pages, but it\'s used in many non-browser environments as well. It is a prototype-based, multi-paradigm scripting language that is dynamic, and supports object-oriented, imperative, and functional programming styles.',
		image: require('images/skills/js.svg'),
		stars: 4.5
	},
	{
		name: 'MySQL',
		description: 'MySQL is the world\'s most popular open source database. With its proven performance, reliability and ease-of-use, MySQL has become the leading database choice for web-based applications, used by high profile web properties including Facebook, Twitter, YouTube, Yahoo! and many more.',
		image: require('images/skills/mysql.png'),
		stars: 4.5
	},
	{
		name: 'PHP',
		description: 'PHP (recursive acronym for PHP: Hypertext Preprocessor) is a widely-used open source general-purpose scripting language that is especially suited for web development and can be embedded into HTML.',
		image: require('images/skills/php.png'),
		stars: 5
	},
	{
		name: 'Sass',
		description: 'Sass is a scripting language that is compiled into Cascading Style Sheets (CSS). It allows for writing your CSS in a nested structure',
		image: require('images/skills/sass.png'),
		stars: 4
	},
	{
		name: 'Typescript',
		description: 'TypeScript is a free and open-source programming language developed and maintained by Microsoft. It is a strict syntactical superset of JavaScript, and adds optional static typing to the language. Anders Hejlsberg, lead architect of C# and creator of Delphi and Turbo Pascal, has worked on the development of TypeScript. TypeScript may be used to develop JavaScript applications for client-side or server-side (Node.js) execution.',
		image: require('images/skills/typescript.png'),
		stars: 0.5
	},
	{
		name: 'Webpack 3',
		description: 'Webpack is a build tool that puts all of your assets, including Javascript, images, fonts, and CSS, in a dependency graph. Webpack lets you use require() in your source code to point to local files, like images, and decide how they\'re processed in your final Javascript bundle, like replacing the path with a URL pointing to a CDN.',
		image: require('images/skills/webpack.svg'),
		stars: 3
	}
];

@Injectable()
export class DataSkillsDataService {

	constructor() { }

	get(): Observable<Skill[]> {
		return of(SKILLS)
	}

}