import { Component, Input, OnInit } from '@angular/core';
import { Skill } from "../../../data/skills/skill.model";

@Component({
	selector: 'skills-skill',
	templateUrl: './templates/skill.html',
	styleUrls: ['./styles/skill.scss']
})
export class PagesSkillsSkillComponent implements OnInit {

	@Input()
	skill: Skill = null;
	stars = [];

	ngOnInit() {

		/* Start at 1 to have index equal to star number */
		for (let index = 1; index < 6; index++) {
			if (this.skill.stars >= index) {
				this.stars.push('gold-star fas fa-star');
				continue;
			}
			if (this.skill.stars < index && this.skill.stars > (index - 1)) {
				this.stars.push('gold-star fas fa-star-half');
				continue;
			}
			this.stars.push('');
		}
	}
}