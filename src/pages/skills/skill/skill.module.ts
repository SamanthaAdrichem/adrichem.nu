import { NgModule } from '@angular/core';
import { CommonModule } from "@angular/common";

import { PagesSkillsSkillComponent } from "./skill.component";

@NgModule({
	imports: [
		CommonModule
	],
	declarations: [
		PagesSkillsSkillComponent
	],
	exports: [
		PagesSkillsSkillComponent
	]
})
export class PagesSkillsSkillModule {}