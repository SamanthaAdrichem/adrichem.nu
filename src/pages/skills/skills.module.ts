import { CommonModule } from "@angular/common";
import { FormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';

import { DataSkillsDataModule } from "../../data/skills/skills-data.module";
import { ElementsContentContainerModule } from "../../elements/content-container/content-container.module";
import { ElementsPageTitleModule } from "../../elements/page-title/page-title.module";
import { PagesSkillsComponent } from "./skills.component";
import { PagesSkillsRoutes } from "./skills.routes";
import { PagesSkillsSkillModule } from "./skill/skill.module";

@NgModule({
	imports: [
		CommonModule,
		FormsModule,

		DataSkillsDataModule,
		ElementsContentContainerModule,
		ElementsPageTitleModule,
		PagesSkillsSkillModule,
		PagesSkillsRoutes
	],
	declarations: [
		PagesSkillsComponent
	],
	exports: []
})
export class PagesSkillsModule {}