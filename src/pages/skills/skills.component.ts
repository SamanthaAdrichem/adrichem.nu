import { Component, OnInit } from '@angular/core';

import { Skill } from "../../data/skills/skill.model";
import { DataSkillsDataService } from "../../data/skills/skills-data.service";

@Component({
	templateUrl: './templates/skills.html',
	styleUrls: ['./styles/skills.scss']
})
export class PagesSkillsComponent implements OnInit {

	private skills: Skill[];
	protected displaySkills: Skill[];
	private searchValue: string = "";
	private skillsDataService: DataSkillsDataService = null;

	constructor(skillsDataService: DataSkillsDataService) {
		this.skillsDataService = skillsDataService;
	}

	public ngOnInit() {
		this.getSkills();
	}

	private getSkills(): void {
		this.skillsDataService.get()
			.subscribe( this.processSkills.bind(this) );
	}

	private processSkills(skills: Skill[]): void {
		this.skills = skills;
		this.filterItem();
	}

	private filterItem(): void {
		this.displaySkills = this.skills
			.filter( this.skillFilter.bind(this) )
			.sort( this.skillSort )
	}

	private skillFilter(item: Skill) : boolean {
		let numericValue = parseFloat(this.searchValue);
		if (false === isNaN(numericValue)) {
			return item.stars >= numericValue;
		}
		return -1 !== item.name.toLowerCase().indexOf(this.searchValue.toLowerCase());
	}

	private skillSort(n1: Skill, n2: Skill): number {
		return n1.name.toLowerCase() > n2.name.toLowerCase() ? 1 : -1;
	}

}