import { RouterModule, Routes} from "@angular/router";
import { PagesSkillsComponent } from "./skills.component";

const routes: Routes = [
	{
		path: 'skills',
		pathMatch: 'full',
		component: PagesSkillsComponent,
		children: []
	}
];

export const PagesSkillsRoutes = RouterModule.forRoot( routes );