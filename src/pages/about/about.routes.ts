import { RouterModule, Routes} from "@angular/router";
import { PagesAboutComponent } from "./about.component";

const routes: Routes = [
	{
		path: 'about',
		pathMatch: 'full',
		component: PagesAboutComponent,
		children: []
	}
];

export const PagesAboutRoutes = RouterModule.forRoot( routes );