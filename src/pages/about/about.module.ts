import { NgModule } from '@angular/core';
import { PagesAboutComponent } from "./about.component";
import { PagesAboutRoutes } from "./about.routes";

import { ElementsContactProfilesModule } from "../../elements/contact-profiles/contact-profiles.module";
import { ElementsContentContainerModule } from "../../elements/content-container/content-container.module";
import { ElementsPageTitleModule } from "../../elements/page-title/page-title.module";

@NgModule({
	imports: [
		PagesAboutRoutes,

		ElementsContactProfilesModule,
		ElementsContentContainerModule,
		ElementsPageTitleModule
	],
	declarations: [
		PagesAboutComponent
	],
	exports: []
})
export class PagesAboutModule {}