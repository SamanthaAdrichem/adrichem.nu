import {Component, OnInit} from '@angular/core';

@Component({
	templateUrl: './templates/about.html',
	styleUrls: ['./styles/about.scss']
})
export class PagesAboutComponent implements OnInit {

	private currentAge: number = 0;

	public ngOnInit() {
		this.currentAge = new Date().getFullYear() - 1985;
	}
}