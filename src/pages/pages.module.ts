import { NgModule } from '@angular/core';
import { PagesAboutModule } from "./about/about.module";
import { PagesContactModule } from "./contact/contact.module";
import { PagesHomeModule } from "./home/home.module";
import { PagesSkillsModule } from "./skills/skills.module";

@NgModule({
	imports: [
		PagesAboutModule,
		PagesContactModule,
		PagesHomeModule,
		PagesSkillsModule
	],
})
export class PagesModule { }