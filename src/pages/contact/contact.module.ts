import { FormsModule }   from '@angular/forms';
import { NgModule } from '@angular/core';
import { CommonModule } from "@angular/common";

import { RecaptchaModule } from 'ng-recaptcha';
import { RecaptchaFormsModule } from "ng-recaptcha/forms";

import { PagesContactComponent } from "./contact.component";
import { PagesContactRoutes } from "./contact.routes";

import { DataContactDataModule } from "../../data/contact/contact-data.module";
import { ElementsContactProfilesModule } from "../../elements/contact-profiles/contact-profiles.module";
import { ElementsContentContainerModule } from "../../elements/content-container/content-container.module";
import { ElementsLoadingContainerModule } from "../../elements/loading-container/loading-container.module";
import { ElementsPageTitleModule } from "../../elements/page-title/page-title.module";

@NgModule({
	imports: [
		FormsModule,
		CommonModule,

		RecaptchaModule.forRoot(),
		RecaptchaFormsModule,

		DataContactDataModule,

		PagesContactRoutes,

		ElementsContactProfilesModule,
		ElementsContentContainerModule,
		ElementsLoadingContainerModule,
		ElementsPageTitleModule
	],
	declarations: [
		PagesContactComponent
	],
	exports: []
})
export class PagesContactModule {}