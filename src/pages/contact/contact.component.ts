import {Component, ViewChild} from '@angular/core';
import { Contact } from "../../data/contact/contact.model";
import { DataContactDataService } from "../../data/contact/contact-data.service";
import {NgForm} from "@angular/forms";

@Component({
	templateUrl: './templates/contact.html',
	styleUrls: ['./styles/contact.scss']
})
export class PagesContactComponent {

	@ViewChild('contactForm')
	public contactForm: NgForm;

	protected contactDataService: DataContactDataService = null;
	protected model = new Contact();

	protected isLoading: boolean = false;
	protected saveSuccessful: boolean = false;
	protected captchaError: boolean = false;

	protected errorMessage: string = "";

	constructor(contactDataService: DataContactDataService) {
		this.contactDataService = contactDataService;
	}

	onSubmit() {
		this.errorMessage = "";
		this.captchaError = !this.model['g-recaptcha-response'];
		if (!this.contactForm.valid || !this.model['g-recaptcha-response']) {
			return;
		}
		this.isLoading = true;
		setTimeout(this.contactMe.bind(this), 200);
	}

	contactMe() {
		this.contactDataService.saveContact( this.model )
			.subscribe(
				this.contactSuccess.bind( this ),
				this.contactFail.bind( this )
			);
	}

	contactSuccess() {
		this.saveSuccessful = true;
		setTimeout( this.stopLoading.bind(this), 500 );
	}

	contactFail( response: any ) {
		this.errorMessage = response && response.error && response.error.title
			? response.error.title
			: "Unknown error";
		setTimeout( this.stopLoading.bind(this), 500 );
	}

	stopLoading() {
		this.isLoading = false;
	}

}