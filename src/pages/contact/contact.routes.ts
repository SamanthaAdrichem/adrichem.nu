import { RouterModule, Routes} from "@angular/router";
import { PagesContactComponent } from "./contact.component";

const routes: Routes = [
	{
		path: 'contact',
		pathMatch: 'full',
		component: PagesContactComponent,
		children: []
	}
];

export const PagesContactRoutes = RouterModule.forRoot( routes );