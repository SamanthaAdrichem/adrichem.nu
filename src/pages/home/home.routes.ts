import { RouterModule, Routes} from "@angular/router";
import { PagesHomeComponent } from "./home.component";

const routes: Routes = [
	{
		path: '',
		pathMatch: 'full',
		component: PagesHomeComponent,
		children: []
	}
];

export const PagesHomeRoutes = RouterModule.forRoot( routes );