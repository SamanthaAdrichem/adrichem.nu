import { CommonModule } from "@angular/common";
import { NgModule } from '@angular/core';

import { PagesHomeComponent } from "./home.component";
import { PagesHomeRoutes } from "./home.routes";

import { DataBlogDataModule } from "../../data/blog/blog-data.module";
import { ElementsContentContainerModule } from "../../elements/content-container/content-container.module";
import { ElementsPageTitleModule } from "../../elements/page-title/page-title.module";
import { PagesHomeBlogItemModule } from "./blog-item/blog-item.module";


@NgModule({
	imports: [
		CommonModule,

		ElementsContentContainerModule,
		ElementsPageTitleModule,
		PagesHomeRoutes,
		PagesHomeBlogItemModule,

		DataBlogDataModule
	],
	declarations: [
		PagesHomeComponent
	],
	exports: []
})
export class PagesHomeModule {}