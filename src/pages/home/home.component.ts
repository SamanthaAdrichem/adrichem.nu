import {Component, OnInit} from '@angular/core';
import {DataBlogDataService} from "../../data/blog/blog-data.service";
import {BlogItem} from "../../data/blog/blog-item.model";

@Component({
	templateUrl: './templates/home.html',
	styleUrls: ['./styles/home.scss']
})
export class PagesHomeComponent implements OnInit {

	private blogItems: BlogItem[];
	private blogDataService: DataBlogDataService = null;

	constructor(blogDataService: DataBlogDataService) {
		this.blogDataService = blogDataService;
	}

	public ngOnInit() {
		this.getBlogItems();
	}

	private getBlogItems(): void {
		this.blogDataService.get()
			.subscribe( this.processBlogItems.bind(this) );
	}

	private processBlogItems(blogItems: BlogItem[]): void {
		this.blogItems = blogItems.sort( this.blogItemsSort );
	}

	private blogItemsSort(n1: BlogItem, n2: BlogItem): number {
		return n1.date.getTime() - n2.date.getTime();
	}

}