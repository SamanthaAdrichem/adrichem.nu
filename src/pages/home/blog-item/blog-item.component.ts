import {Component, Input, OnInit} from '@angular/core';
import { BlogItem } from "../../../data/blog/blog-item.model";
import {DomSanitizer, SafeHtml} from '@angular/platform-browser';

@Component({
	selector: 'home-blog-item',
	templateUrl: './templates/blog-item.html',
	styleUrls: ['./styles/blog-item.scss']
})
export class PagesHomeBlogItemComponent implements OnInit{

	private domSanitizer: DomSanitizer = null;
	protected showAll: boolean = false;
	protected moreLessText: string = "Read more";
	protected description: SafeHtml = null;

	@Input()
	blogItem: BlogItem = null;

	constructor(domSanitizer: DomSanitizer) {
		this.domSanitizer = domSanitizer;
	}

	ngOnInit() {
		this.description = this.domSanitizer.bypassSecurityTrustHtml( this.blogItem.description );
	}

	readMoreLess() {
		if (this.showAll) {
			this.moreLessText = "Read more";
			this.showAll = false;
			return;
		}

		this.moreLessText = "Read less";
		this.showAll = true;
	}

}