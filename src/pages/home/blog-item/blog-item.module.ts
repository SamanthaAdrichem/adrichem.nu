import { NgModule } from '@angular/core';
import { CommonModule } from "@angular/common";

import { ElementsContentContainerModule } from "../../../elements/content-container/content-container.module";
import { PagesHomeBlogItemComponent } from "./blog-item.component";

@NgModule({
	imports: [
		CommonModule,

		ElementsContentContainerModule
	],
	declarations: [
		PagesHomeBlogItemComponent
	],
	exports: [
		PagesHomeBlogItemComponent
	]
})
export class PagesHomeBlogItemModule {}