import { Component, Input } from '@angular/core';

@Component({
	selector: 'elements-page-title',
	templateUrl: './templates/page-title.html',
	styleUrls: ['./styles/page-title.scss']
})
export class ElementsPageTitleComponent {

	@Input()
	pageTitle: string = "";
}