import { NgModule } from '@angular/core';
import { CommonModule } from "@angular/common";

import { ElementsPageTitleComponent } from "./page-title.component";

@NgModule({
	imports: [
		CommonModule
	],
	declarations: [
		ElementsPageTitleComponent
	],
	exports: [
		ElementsPageTitleComponent
	]
})
export class ElementsPageTitleModule {}