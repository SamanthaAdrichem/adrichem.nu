import { Component, Input } from '@angular/core';

@Component({
	selector: 'elements-contact-profiles',
	templateUrl: './templates/contact-profiles.html',
	styleUrls: ['./styles/contact-profiles.scss']
})
export class ElementsContactProfilesComponent {

	@Input()
	contactProfiles: string = "";
}