import { NgModule } from '@angular/core';
import { CommonModule } from "@angular/common";

import { ElementsContactProfilesComponent } from "./contact-profiles.component";

@NgModule({
	imports: [
		CommonModule
	],
	declarations: [
		ElementsContactProfilesComponent
	],
	exports: [
		ElementsContactProfilesComponent
	]
})
export class ElementsContactProfilesModule {}