import { Component, Input } from '@angular/core';

@Component({
	selector: 'elements-loading-container',
	templateUrl: './templates/loading-container.html',
	styleUrls: ['./styles/loading-container.scss']
})
export class ElementsLoadingContainerComponent {

	@Input()
	loadingText: string = "Loading";

	@Input()
	isLoading: boolean = false;
}