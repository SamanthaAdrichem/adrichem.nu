import { NgModule } from '@angular/core';
import { CommonModule } from "@angular/common";

import { ElementsLoadingContainerComponent } from "./loading-container.component";

@NgModule({
	imports: [
		CommonModule
	],
	declarations: [
		ElementsLoadingContainerComponent
	],
	exports: [
		ElementsLoadingContainerComponent
	]
})
export class ElementsLoadingContainerModule {}