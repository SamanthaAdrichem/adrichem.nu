import { Component, Input } from '@angular/core';

@Component({
	selector: 'elements-content-container',
	templateUrl: './templates/content-container.html',
	styleUrls: ['./styles/content-container.scss']
})
export class ElementsContentContainerComponent {

	@Input()
	background: boolean = true;
}