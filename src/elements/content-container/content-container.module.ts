import { NgModule } from '@angular/core';
import { CommonModule } from "@angular/common";

import { ElementsContentContainerComponent } from "./content-container.component";

@NgModule({
	imports: [
		CommonModule
	],
	declarations: [
		ElementsContentContainerComponent
	],
	exports: [
		ElementsContentContainerComponent
	]
})
export class ElementsContentContainerModule {}